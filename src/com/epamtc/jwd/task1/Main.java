package com.epamtc.jwd.task1;

import com.epamtc.jwd.task1.exception.NullPointerException;
import com.epamtc.jwd.task1.exception.WrongLengthException;
import com.epamtc.jwd.task1.exception.WrongPositionException;
import com.epamtc.jwd.task1.util.CharsStringManager;
import com.epamtc.jwd.task1.util.PlainStringManager;
import com.epamtc.jwd.task1.util.RegexStringManager;
import com.epamtc.jwd.task1.util.StringManager;

public class Main {
    public static void main(String[] args) {
        StringManager[] managers = {new RegexStringManager(), new PlainStringManager()};
        String[] testStrings = {"qwerty asdfg zxcv",
                                "text textp textpo textpa",
                                "q qw qwe qwer a as asd asdf z zx zxc zxcv",
                                "qwerty йцукен1234asd",
                                "asd zxc text"};
        for (StringManager util : managers) {
            System.out.println(util.getClass().getSimpleName());
            try {
                System.out.println(testStrings[0] + " -> " + util.changeKthLetter(5, testStrings[0], '1'));
            } catch (NullPointerException | WrongPositionException e) {
                //log
            }
            try {
                System.out.println(testStrings[1] + " -> " + util.fix(testStrings[1]));
            } catch (NullPointerException e) {
                //log
            }
            try {
                System.out.println(testStrings[2] + " -> " + util.replace(2, testStrings[2], " new "));
            } catch (NullPointerException | WrongLengthException e) {
                //log
            }
            try {
                System.out.println(testStrings[3] + " -> " + util.filterSymbols(testStrings[3]));
            } catch (NullPointerException e) {
                //log
            }
            try {
                System.out.println(testStrings[4] + " -> " + util.filterWords(3, testStrings[4]));
            } catch (NullPointerException | WrongLengthException e) {
                //log
            }
            System.out.println();
        }

        CharsStringManager util = new CharsStringManager();
        System.out.println(util.getClass().getSimpleName());
        try {
            System.out.println(testStrings[0] + " -> " + util.changeKthLetter(5, testStrings[0].toCharArray(), '1'));
        } catch (NullPointerException | WrongPositionException e) {
            //log
        }
        try {
            System.out.println(testStrings[1] + " -> " + util.fix(testStrings[1].toCharArray()));
        } catch (NullPointerException e) {
            //log
        }
        try {
            System.out.println(testStrings[2] + " -> " + util.replace(2, testStrings[2].toCharArray(), " new "));
        } catch (NullPointerException | WrongLengthException e) {
            //log
        }
        try {
            System.out.println(testStrings[3] + " -> " + util.filterSymbols(testStrings[3].toCharArray()));
        } catch (NullPointerException e) {
            //log
        }
        try {
            System.out.println(testStrings[4] + " -> " + util.filterWords(3, testStrings[4].toCharArray()));
        } catch (NullPointerException | WrongLengthException e) {
            //log
        }
        System.out.println();
    }
}
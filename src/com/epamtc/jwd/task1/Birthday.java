package com.epamtc.jwd.task1;

import java.util.Calendar;

class Birthday {
    public static void main(String[] args){
        if (args.length != 3){
            System.out.println("Wrong args count");
            return;
        }
        int day;
        try {
            day = Integer.parseInt(args[0]);
        }catch (NumberFormatException e){
            System.out.println("Arguments must be integers");
            return;
        }
        int month;
        try {
            month = Integer.parseInt(args[1]);
        }catch (NumberFormatException e){
            System.out.println("Arguments must be integers");
            return;
        }
        int year;
        try {
            year = Integer.parseInt(args[2]);
        }catch (NumberFormatException e){
            System.out.println("Arguments must be integers");
            return;
        }
        if (!isDateCorrect(day, month, year)){
            System.out.println("Wrong date of birth");
            return;
        }
        Calendar now = Calendar.getInstance();

        Calendar birthday = Calendar.getInstance();
        birthday.set(year,month-1,day);

        int years = diff(now, birthday);
        if (years<0){
            System.out.println("Wrong date of birth");
            return;
        }

        System.out.println(dayOfWeek(birthday));
        System.out.println("This person is "+years+" years old");
        if (birthday.get(Calendar.DAY_OF_MONTH)==now.get(Calendar.DAY_OF_MONTH) &&
            birthday.get(Calendar.MONTH)==now.get(Calendar.MONTH))
            System.out.println("Happy birthday");
    }

    private static boolean isDateCorrect(int day, int month, int year){
        try {
            Calendar birthday = Calendar.getInstance();
            birthday.set(year,month-1,day);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    private static int diff(Calendar second, Calendar first){
        int res = second.get(Calendar.YEAR)-first.get(Calendar.YEAR);
        if (second.get(Calendar.MONTH)>first.get(Calendar.MONTH) ||
            (second.get(Calendar.MONTH)==first.get(Calendar.MONTH) && second.get(Calendar.DAY_OF_MONTH)>first.get(Calendar.DAY_OF_MONTH)))
            res--;
        return res;
    }

    private static String dayOfWeek(Calendar day){
        switch (day.get(Calendar.DAY_OF_WEEK)){
            case 1:return "Sunday";
            case 2:return "Monday";
            case 3:return "Tuesday";
            case 4:return "Wednesday";
            case 5:return "Thursday";
            case 6:return "Friday";
            case 7:return "Saturday";
            default:return "Wrong day";
        }
    }
}
package com.epamtc.jwd.task1.exception;

public class NullPointerException extends Exception{
    public NullPointerException() {
        super();
    }

    public NullPointerException(String message) {
        super(message);
    }

    public NullPointerException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullPointerException(Throwable cause) {
        super(cause);
    }
}

package com.epamtc.jwd.task1.util;

import com.epamtc.jwd.task1.exception.NullPointerException;
import com.epamtc.jwd.task1.exception.WrongLengthException;
import com.epamtc.jwd.task1.exception.WrongPositionException;

public class PlainStringManager extends StringManager{
    @Override
    public String changeKthLetter(int k, String input, char newbie) throws NullPointerException, WrongPositionException {
        if (input==null) throw new NullPointerException();
        if (k<0) throw new WrongPositionException();
        k--;

        String[] words = input.split(" ");
        String[] res = new String[words.length];
        System.arraycopy(words, 0, res, 0, words.length);

        for (int i=0; i<words.length; i++)
            if (k<words[i].length())
                res[i] = words[i].substring(0,k) + newbie + words[i].substring(k+1);

        StringBuilder sb = new StringBuilder();
        for (String word:res)
            sb.append(word).append(" ");

        return sb.toString();
    }

    @Override
    public String fix(String input) throws NullPointerException {
        if (input==null) throw new NullPointerException();

        String[] words = input.split(" ");
        String[] res = new String[words.length];
        System.arraycopy(words, 0, res, 0, words.length);

        for (int i=0; i<res.length; i++)
            for (int j=0; j<res[i].length()-1; j++)
                if (res[i].charAt(j)=='P' || res[i].charAt(j)=='p')
                    if (res[i].charAt(j+1)=='A')
                        res[i] = res[i].substring(0, j+1) + 'O' + res[i].substring(j + 2);
                    else if (res[i].charAt(j+1)=='a')
                        res[i] = res[i].substring(0, j+1) + 'o' + res[i].substring(j + 2);

        StringBuilder sb = new StringBuilder();
        for (String word:res)
            sb.append(word).append(" ");

        return sb.toString();
    }

    @Override
    public String replace(int len, String input, String newbie) throws NullPointerException, WrongLengthException {
        if (input==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return input;
        if (newbie==null) return input;

        String[] words = input.split(" ");
        String[] res = new String[words.length];
        System.arraycopy(words, 0, res, 0, words.length);

        for (int i=0; i<words.length; i++)
            if (len == words[i].length())
                res[i] = newbie;

        StringBuilder sb = new StringBuilder();
        for (String word:res)
            sb.append(word).append(" ");

        return sb.toString();
    }

    @Override
    public String filterSymbols(String input) throws NullPointerException {
        if (input==null) throw new NullPointerException();

        StringBuilder sb = new StringBuilder();

        for (int i=0; i<input.length(); i++)
            if (Character.isLetter(input.charAt(i)) || input.charAt(i)==' ')
                sb.append(input.charAt(i));
            else
                sb.append(' ');

        return sb.toString();
    }

    @Override
    public String filterWords(int len, String input) throws NullPointerException, WrongLengthException {
        if (input==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return input;

        String[] words = input.split(" ");
        String[] res = new String[words.length];
        System.arraycopy(words, 0, res, 0, words.length);

        for (int i=0; i<words.length; i++)
            if (len == words[i].length() && isConsonant(words[i].charAt(0)))
                res[i] = "";

        StringBuilder sb = new StringBuilder();
        for (String word:res)
            if (word.length()>0)
                sb.append(word).append(" ");
            else
                sb.append(" ");

        return sb.toString();
    }
}

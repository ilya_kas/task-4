package com.epamtc.jwd.task1.util;

import com.epamtc.jwd.task1.exception.NullPointerException;
import com.epamtc.jwd.task1.exception.WrongLengthException;
import com.epamtc.jwd.task1.exception.WrongPositionException;

import java.util.ArrayList;
import java.util.List;

public abstract class StringManager {
    final static List<Character> VOVAL = new ArrayList<>();
    static {
        VOVAL.add('A');
        VOVAL.add('E');
        VOVAL.add('I');
        VOVAL.add('O');
        VOVAL.add('U');
        VOVAL.add('Y');
        VOVAL.add('a');
        VOVAL.add('e');
        VOVAL.add('i');
        VOVAL.add('o');
        VOVAL.add('u');
        VOVAL.add('y');
    }

    public abstract String changeKthLetter(int k, String input, char newbie) throws NullPointerException, WrongPositionException;
    public abstract String fix(String input) throws NullPointerException;
    public abstract String replace(int len, String input, String newbie) throws NullPointerException, WrongLengthException;
    public abstract String filterSymbols(String input) throws NullPointerException;
    public abstract String filterWords(int len, String input) throws NullPointerException, WrongLengthException;

    protected boolean isConsonant(char letter){
        return !VOVAL.contains(letter);
    }
}

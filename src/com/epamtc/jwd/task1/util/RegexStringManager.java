package com.epamtc.jwd.task1.util;

import com.epamtc.jwd.task1.exception.NullPointerException;
import com.epamtc.jwd.task1.exception.WrongLengthException;
import com.epamtc.jwd.task1.exception.WrongPositionException;

public class RegexStringManager extends StringManager{
    @Override
    public String changeKthLetter(int k, String input, char newbie) throws NullPointerException, WrongPositionException {
        if (input==null) throw new NullPointerException();
        if (k<0) throw new WrongPositionException();

        String[] words = input.split(" ");
        String[] res = new String[words.length];
        System.arraycopy(words, 0, res, 0, words.length);

        for (int i=0; i<words.length; i++)
            res[i] = words[i].replaceFirst("(?<=.{" + (k - 1) + "}).(?=.*)", String.valueOf(newbie));

        StringBuilder sb = new StringBuilder();
        for (String word:res)
            sb.append(word).append(" ");

        return sb.toString();
    }

    @Override
    public String fix(String input) throws NullPointerException {
        if (input==null) throw new NullPointerException();

        return input.replaceAll("[pP][aA]","po");
    }

    @Override
    public String replace(int len, String input, String newbie) throws NullPointerException, WrongLengthException {
        if (input==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return input;
        if (newbie==null) return input;

        return input.replaceAll("(?<=\\s|^)[A-Za-z]{" + len + "}(?=\\s|$)", newbie);
    }

    @Override
    public String filterSymbols(String input) throws NullPointerException {
        if (input==null) throw new NullPointerException();

        return input.replaceAll("[^ A-Za-z]", "");
    }

    @Override
    public String filterWords(int len, String input) throws NullPointerException, WrongLengthException {
        if (input==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return input;

        return input.replaceAll("(?<=\\s|^)[BbCcDdFfGgHhJjKkLlMmNnPpQqRrSsTtVvWwXxYyZz][A-Za-z]{" + (len - 1) + "}(?=\\s|$)", "");
    }
}

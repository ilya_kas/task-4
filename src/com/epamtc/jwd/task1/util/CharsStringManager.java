package com.epamtc.jwd.task1.util;

import com.epamtc.jwd.task1.exception.NullPointerException;
import com.epamtc.jwd.task1.exception.WrongLengthException;
import com.epamtc.jwd.task1.exception.WrongPositionException;

import java.util.ArrayList;
import java.util.List;

public class CharsStringManager{

    public String changeKthLetter(int k, char[] text, char newbie) throws NullPointerException, WrongPositionException {
        if (text==null) throw new NullPointerException();
        if (k<0) throw new WrongPositionException();

        StringBuilder sb = new StringBuilder();
        int p=0; //указатель на начало слова

        for (int i=0; i<text.length; i++){
            if (text[i]==' '){
                sb.append(text[i]);
                p=i+1;
                continue;
            }
            if (k==(i-p+1))
                sb.append(newbie);
            else
                sb.append(text[i]);
        }

        return sb.toString();
    }

    public String fix(char[] text) throws NullPointerException {
        if (text==null) throw new NullPointerException();

        StringBuilder sb = new StringBuilder();

        if (text.length>0)
            sb.append(text[0]);
        for (int i=1; i<text.length; i++)
            if (text[i - 1] == 'P' || text[i - 1] == 'p'){
                if (text[i] == 'A')
                    sb.append('O');
                else if (text[i] == 'a')
                    sb.append('o');
                else
                    sb.append(text[i]);
            }else
                sb.append(text[i]);


        return sb.toString();
    }

    public String replace(int len, char[] text, String newbie) throws NullPointerException, WrongLengthException {
        if (text==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return String.copyValueOf(text);
        if (newbie==null) return String.copyValueOf(text);

        StringBuilder sb = new StringBuilder();
        int p=0; //указатель на начало слова

        for (int i=0; i<text.length; i++){
            if (text[i]==' '){
                if ((i-p)==len)
                    sb.append(newbie);
                else
                    for (int j=p;j<i;j++)
                        sb.append(text[j]);
                sb.append(text[i]);
                p=i+1;
            }
        }
        for (int j=p;j<text.length;j++)
            sb.append(text[j]);

        return sb.toString();
    }

    public String filterSymbols(char[] text) throws NullPointerException {
        if (text==null) throw new NullPointerException();

        StringBuilder sb = new StringBuilder();

        for (int i=0; i<text.length; i++)
            if (Character.isLetter(text[i]) || text[i]==' ')
                sb.append(text[i]);
            else
                sb.append(' ');

        return sb.toString();
    }

    public String filterWords(int len, char[] text) throws NullPointerException, WrongLengthException {
        if (text==null) throw new NullPointerException();
        if (len<0) throw new WrongLengthException();
        if (len==0) return String.copyValueOf(text);

        StringBuilder sb = new StringBuilder();
        int p=0; //указатель на начало слова

        for (int i=0; i<text.length; i++){
            if (text[i]==' '){
                if ((i-p)!=len || !isConsonant(text[p]))
                    for (int j = p; j < i; j++)
                        sb.append(text[j]);
                sb.append(text[i]);
                p=i+1;
            }
        }
        for (int j=p;j<text.length;j++)
            sb.append(text[j]);

        return sb.toString();
    }

    final static List<Character> VOVAL = new ArrayList<>();
    static {
        VOVAL.add('A');
        VOVAL.add('E');
        VOVAL.add('I');
        VOVAL.add('O');
        VOVAL.add('U');
        VOVAL.add('Y');
        VOVAL.add('a');
        VOVAL.add('e');
        VOVAL.add('i');
        VOVAL.add('o');
        VOVAL.add('u');
        VOVAL.add('y');
    }

    private boolean isConsonant(char letter){
        return !VOVAL.contains(letter);
    }
}
